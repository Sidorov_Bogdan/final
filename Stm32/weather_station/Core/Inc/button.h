#ifndef EXERCISE05_BUTTON_H
#define EXERCISE05_BUTTON_H

#include <stdint-gcc.h>

#define USER_BTN_GPIO_Pin GPIO_PIN_0
#define USER_BTN_GPIO_Port GPIOA

/*
 * delay to suppress rattling in ms
 */
#define BTN_USER_RATTLING_DELAY 50
/*
 * Time is needed to set the "HELD" status
 */
#define BTN_USER_HOLD_DELAY 3000

typedef enum { BTN_USER } BTN_Pin;

typedef enum {
	BTN_STATUS_PRESSED,
	BTN_STATUS_UNPRESSED,
	/*
     * the button has been held more than 3 seconds
     */
	BTN_STATUS_HELD,
	BTN_STATUS_RELEASED
} BTN_Status;

/*
 *  This function returns the current status of passed button
 */
BTN_Status BTN_ReadState(BTN_Pin buttonPin);
void BTN_ProcessIRQ(BTN_Pin buttonPin);

#endif //EXERCISE05_BUTTON_H
