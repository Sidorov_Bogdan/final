#ifndef WEATHER_STATION_WIFI_H
#define WEATHER_STATION_WIFI_H

#include "main.h"
#include <stdlib.h>

#define BUFFER_SIZE 128

#define RECEIVE_TIMEOUT_MS 10000
#define TRANSMIT_TIMEOUT_MS 2000
#define ATTEMPTS_COUNT 5

typedef enum {
	/* The last operation (or sequence of operations) has been done successfully.  */
	WIFI_OK,
	/* The last operation has occurred an error (overtime, ...)*/
	WIFI_ERROR,
	/* The count of attempts to try to execute the last command was run out. */
	WIFI_ATTEMPTS_END,
	/* If the response hasn't passed validation for the expected value */
	WIFI_NOT_VALID_RESPONSE,
	//    /* The device hasn't been initialized yet */
	//    WIFI_NOT_INITIALIZED
} wifi_state;

struct wifi_t {
	UART_HandleTypeDef *huart;
	char *buffer;
	char *buffer_command;
	uint8_t is_wifi_connected;
	wifi_state device_state;
};

extern volatile uint8_t UART_TRANSMIT_CPLT_FLAG;
extern volatile uint8_t UART_RECEIVE_CPLT_FLAG;
extern volatile uint8_t UART_ERROR_FLAG;

wifi_state wifi_init(struct wifi_t *wifi, UART_HandleTypeDef *huart_in);
wifi_state wifi_deinit(struct wifi_t *wifi);
wifi_state wifi_connect(struct wifi_t *wifi, char *ssid, char *pass);
wifi_state wifi_disconnect(struct wifi_t *wifi);
/* Pass a buffer of size 20 or more */
wifi_state wifi_get_ip(struct wifi_t *wifi, char *buffer_out);
wifi_state wifi_send_str_tcp(struct wifi_t *wifi, char *address, char *port,
			     char *str, size_t size);
wifi_state wifi_send_str_to_thingsspeak(struct wifi_t *wifi, float temp, float hum);

#endif //WEATHER_STATION_WIFI_H
