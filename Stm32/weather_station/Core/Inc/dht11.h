#ifndef WEATHER_STATION_DHT11_H
#define WEATHER_STATION_DHT11_H

#include "main.h"

extern volatile uint32_t us_counter;

#define BUFFER_SIZE 5
#define BYTE_SIZE 8

struct dht11_t {
	TIM_HandleTypeDef *tim_one_us;
	GPIO_TypeDef *port;
	uint16_t pin;
	uint8_t buffer[BUFFER_SIZE];
};

struct dht11_t dht11_init(GPIO_TypeDef *port, uint16_t pin,
			  TIM_HandleTypeDef *tim);

void dht11_do_measurement(struct dht11_t *dht11);

float dth11_convert_temp(struct dht11_t *dht11);
uint8_t dth11_convert_hum(struct dht11_t *dht11);

#endif //WEATHER_STATION_DHT11_H
