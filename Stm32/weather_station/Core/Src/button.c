#include "button.h"
#include "main.h"

/*
 * if it's 1, then the last operation was pushing
 * if it's 0, then the last operation was releasing
 */
static uint8_t is_rising;

/*
 * The variable to count the elapsed time
 */

static BTN_Status BTN_User_State = BTN_STATUS_UNPRESSED;

BTN_Status BTN_ReadState(BTN_Pin buttonPin)
{
	if (buttonPin == BTN_USER) {
		/*
         * Wait to suppress rattling effect (wait 50 ms)
         */
		uint32_t start = HAL_GetTick();
		while (1) {
			if ((HAL_GetTick() - start) >=
			    BTN_USER_RATTLING_DELAY) {
				break;
			}
		}

		BTN_Status ret = BTN_User_State;

		if (ret == BTN_STATUS_RELEASED) {
			return BTN_STATUS_RELEASED;
		}

		uint8_t last_state = is_rising;

//		if (last_state == is_rising) {
//			start = HAL_GetTick();
//			while (last_state == is_rising) {
//				if ((HAL_GetTick() - start) >=
//				    BTN_USER_HOLD_DELAY) {
//					BTN_User_State = BTN_STATUS_HELD;
//					ret = BTN_STATUS_HELD;
//					break;
//				}
//			}
//		}

		return ret;
	} else {
		return BTN_STATUS_UNPRESSED;
	}
}

void BTN_ProcessIRQ(BTN_Pin buttonPin)
{
	GPIO_PinState state;
	if (buttonPin == BTN_USER) {
		/*
         * Read the button's state
         */
		state = HAL_GPIO_ReadPin(USER_BTN_GPIO_Port, USER_BTN_GPIO_Pin);

		/*
         *  If the button doesn't change its state and stays unpressed, then leave this state.
         */
		if ((BTN_User_State == BTN_STATUS_UNPRESSED) &&
		    (state == GPIO_PIN_RESET)) {
			BTN_User_State = BTN_STATUS_UNPRESSED;

			/*
             * If the button changes its state and becomes pressed, then set the state of PRESSED
             */
		} else if ((BTN_User_State == BTN_STATUS_UNPRESSED) &&
			   (state == GPIO_PIN_SET)) {
			BTN_User_State = BTN_STATUS_PRESSED;

			/*
             * If the button changes its state and becomes unpressed, then set the state of BTN_RELEASED
             */
		} else if ((BTN_User_State == BTN_STATUS_PRESSED) &&
			   (state == GPIO_PIN_RESET)) {
			BTN_User_State = BTN_STATUS_RELEASED;

			/*
             * If the button is unpressed and the previous state is BTN_HELD, then set the state of BTN_RELEASE
             */
		} else if ((BTN_User_State == BTN_STATUS_HELD) &&
			   (state == GPIO_PIN_RESET)) {
			BTN_User_State = BTN_STATUS_UNPRESSED;

			/*
             *
             */
		} else if ((BTN_User_State == BTN_STATUS_RELEASED) &&
			   (state == GPIO_PIN_SET)) {
			BTN_User_State = BTN_STATUS_PRESSED;

		} else if ((BTN_User_State == BTN_STATUS_RELEASED) &&
			   (state == GPIO_PIN_RESET)) {
			BTN_User_State = BTN_STATUS_UNPRESSED;
		}
	}

	is_rising = !is_rising;
}