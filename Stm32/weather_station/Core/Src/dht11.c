#include "dht11.h"

volatile uint32_t us_counter = 0;

static void delay_us(struct dht11_t *dht11, uint32_t t)
{
	__HAL_TIM_SET_COUNTER(dht11->tim_one_us,
			      0); // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(dht11->tim_one_us) < t)
		; // wait for the counter to reach the us input in the parameter
}

static void init_gpio(struct dht11_t *dht11)
{
	/* Init data pin */
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	GPIO_InitStruct.Pin = dht11->pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(dht11->port, &GPIO_InitStruct);
}

struct dht11_t dht11_init(GPIO_TypeDef *port, uint16_t pin,
			  TIM_HandleTypeDef *tim)
{
	struct dht11_t ret;
	ret.port = port;
	ret.pin = pin;
	ret.tim_one_us = tim;

	init_gpio(&ret);

	HAL_Delay(3000);
	/* Set high level to let the sensor coming to persistent state */
	HAL_GPIO_WritePin(ret.port, ret.pin, GPIO_PIN_SET);

	/* Start 1us timer */
	HAL_TIM_Base_Start(tim);

	return ret;
}

void dht11_do_measurement(struct dht11_t *dht11)
{
	/* Reset data pin and wait 100 ms */
	HAL_GPIO_WritePin(dht11->port, dht11->pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(dht11->port, dht11->pin, GPIO_PIN_SET);
	delay_us(dht11, 50000);
	delay_us(dht11, 50000);

	/* Send start command */
	HAL_GPIO_WritePin(dht11->port, dht11->pin, GPIO_PIN_RESET);
	delay_us(dht11, 18000);
	HAL_GPIO_WritePin(dht11->port, dht11->pin, GPIO_PIN_SET);

	/* Wait for a response */
	delay_us(dht11, 39);

	/* If the sensor doesn't pull down pin's state - it means no response */
	if (HAL_GPIO_ReadPin(dht11->port, dht11->pin) == GPIO_PIN_SET) {
		return;
	}

	/* Wait again */
	delay_us(dht11, 80);

	/* If the sensor doesn't release pin's state - it means no response (maybe the device isn't ready yet) */
	if (HAL_GPIO_ReadPin(dht11->port, dht11->pin) == GPIO_PIN_RESET) {
		return;
	}

	delay_us(dht11, 80);

	/* Read the data (we store high bytes at first)*/
	for (size_t i = 0; i < BUFFER_SIZE; ++i) {
		/* Set the entire byte to zero */
		dht11->buffer[i] = 0;

		/* Get each byte */
		for (size_t j = 0; j < BYTE_SIZE; ++j) {
			/* Wait till the sensor releases port (50 us delay) */
			while (HAL_GPIO_ReadPin(dht11->port, dht11->pin) ==
			       GPIO_PIN_RESET)
				;

			/* Wait for 30us to know 0 or 1 is being sent */
			delay_us(dht11, 30);

			if (HAL_GPIO_ReadPin(dht11->port, dht11->pin) ==
			    GPIO_PIN_SET) {
				/* Set a one, if the port isn't returned to zero for this time */
				dht11->buffer[i] |= (uint8_t)(
					(uint8_t)1 << ((uint8_t)7 - j));
			}

			/* Wait for a zero (in case of the value of 1) */
			while (HAL_GPIO_ReadPin(dht11->port, dht11->pin) ==
			       GPIO_PIN_SET)
				;
		}
	}
}

float dth11_convert_temp(struct dht11_t *dht11)
{
	/* Get integer part */
	float tmp = (float)dht11->buffer[2];

	/* Get floating part */
	tmp += (float)dht11->buffer[3] / 10;

	return tmp;
}

uint8_t dth11_convert_hum(struct dht11_t *dht11)
{
	return dht11->buffer[0];
}
