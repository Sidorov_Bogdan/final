#include "wifi.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "lcd.h"

#define AT_CHECK_STATE "AT\r\n"
#define AT_DISABLE_ECHO "ATE0\r\n"
#define AT_SET_STATION_MODE "AT+CWMODE_CUR=1\r\n"
#define AT_CONNECT_BASE "AT+CWJAP_CUR="
#define AT_GET_IP "AT+CIFSR\r\n"
#define AT_DISCONNECT "AT+CWQAP\r\n"
#define AT_TCP_CONNECT_BASE "AT+CIPSTART=\"TCP\","
#define AT_TCP_CLOSE "AT+CIPCLOSE\r\n"
#define AT_TCP_SEND_BASE "AT+CIPSEND="
#define AT_TCP_SEND_THINGSSPEAK "AT+CIPSTART=\"TCP\",\"184.106.153.149\",80\r\n"
#define AT_TCP_SET_SINGLE_CON "AT+CIPMUX=0\r\n"

#define THINGSPEAK_API_KEY "42N5KNETO119I6E3"

#define AT_RESPONSE_OK "OK"
#define AT_RESPONSE_WIFI_CONNECTED "WIFI CONNECTED"
#define AT_RESPONSE_TRANSFER_READY ">"

#define CR_CHAR "\r\n"

static uint8_t wait_for_receive_end = 1;

#define __SET_WAIT_FOR_RECEIVE_END__(val) (wait_for_receive_end = val)

volatile uint8_t UART_TRANSMIT_CPLT_FLAG = 0;
volatile uint8_t UART_RECEIVE_CPLT_FLAG = 0;
volatile uint8_t UART_ERROR_FLAG = 0;

static void pull_ip(char *buffer_in, char *ip_out)
{
	char *ip_start = strstr(buffer_in, "192");
	char *ip_end = strchr(buffer_in, '\r');
	/* Exclude '"' symbol */
	ip_end--;

	if (ip_start == NULL || ip_end == NULL) {
		return;
	}

	size_t ip_size = ip_end - ip_start;

	memcpy(ip_out, ip_start, ip_size);

	ip_out[ip_size] = '\0';
}

/* This function deletes all CR characters from the string */
static void pull_response(char *response_out)
{
	/* Find the start index of the substring */
	char *rem_start = strstr(response_out, CR_CHAR);

	/* If no match - just return */
	if (rem_start == NULL) {
		return;
	}

	const size_t rem_len = strlen(CR_CHAR);
	char *copy_end;
	/* The non-CR substring start index (goes after found CR symbols) */
	char *copy_from = rem_start + rem_len;

	/* Find more matches */
	while ((copy_end = strstr(copy_from, CR_CHAR)) != NULL) {
		/* Overwrite CR symbols */
		memmove(rem_start, copy_from, copy_end - copy_from);

		/* Move on the ptrs */
		rem_start += copy_end - copy_from;
		copy_from = copy_end + rem_len;
	}

	/* Overwrite the last CR symbols */
	/* Also including \0 */
	memmove(rem_start, copy_from, 1 + strlen(copy_from));
}

/* This function returns the number of bytes which have been successfully read.
 * It returns the value less than zero if an error occurs or the timeout expires. */
static int16_t uart_receive_response(struct wifi_t *wifi)
{
	/* This function is able to receive data from esp8266, only if the latest operation is successful */
	/* Whatever the latest operation is failed, try to get a response. Because the attempts aren't run out. */
	if (wifi->device_state != WIFI_OK && wifi->device_state != WIFI_ERROR &&
	    wifi->device_state != WIFI_NOT_VALID_RESPONSE) {
		return -1;
	}

	uint8_t prev_byte = 0;
	uint8_t byte = 0;
	uint8_t tmp_byte;
	uint32_t start;
	size_t buff_index_next = 0;

	/* Set all elements of buffer to zero before read attempting */
	memset(wifi->buffer, 0, BUFFER_SIZE);

	/* Read until the meet CR symbols or the buffer space is run out */
	/* Remember to account the determining zero (-1) */
	while (((prev_byte != (uint8_t)'\r' || byte != (uint8_t)'\n') &&
		(buff_index_next < BUFFER_SIZE - 1)) ||
	       buff_index_next == 2 || buff_index_next == 3) {
		HAL_UART_Receive_IT(wifi->huart, &tmp_byte, 1);

		/* Store the timestamp of the waiting start */
		start = HAL_GetTick();

		/* Wait till receive interrupt is called or an error occurs (timeout is over or UART error interrupt is called) */
		while ((UART_RECEIVE_CPLT_FLAG != 1) &&
		       ((HAL_GetTick() - start < RECEIVE_TIMEOUT_MS)) &&
		       (UART_ERROR_FLAG != 1))
			;

		/* Process errors */
		if (UART_RECEIVE_CPLT_FLAG != 1) {
			UART_ERROR_FLAG = 0;
			return -1;
		}

		/* Reset flag */
		UART_RECEIVE_CPLT_FLAG = 0;

		/* Store the byte */
		prev_byte = byte;
		byte = tmp_byte;
		wifi->buffer[buff_index_next] = tmp_byte;

		/* Set the next index */
		++buff_index_next;
	}

	wifi->buffer[buff_index_next] = '\0';
	UART_ERROR_FLAG = 0;

	/* Clear RX buffer and reset all flags related to receiving data to prevent overrun error  */
	HAL_UART_AbortReceive_IT(wifi->huart);

	/* Wait for end of transfer to prevent busy state error */
	if (wait_for_receive_end) {
		char ok_buf[6];
		while (1) {
			memset(ok_buf, 0, 6);
			HAL_UART_Transmit(wifi->huart,
					  (uint8_t *)AT_CHECK_STATE,
					  strlen(AT_CHECK_STATE),
					  TRANSMIT_TIMEOUT_MS);
			HAL_UART_Receive(wifi->huart, (uint8_t *)ok_buf, 6,
					 TRANSMIT_TIMEOUT_MS);
			pull_response(ok_buf);
			if (strstr(ok_buf, AT_RESPONSE_OK) != NULL) {
				break;
			}
			HAL_UART_AbortReceive_IT(wifi->huart);
		}
	}

	/* Clear RX buffer and reset all flags related to receiving data to prevent overrun error  */
	HAL_UART_AbortReceive_IT(wifi->huart);

	return buff_index_next;
}

/* This function tries to transmit data to esp8266 and after that checks for errors */
/* This function returns the number of bytes which have been successfully read as an esp8266 response for the command
 * Or it returns value less than zero if the number of attempts to execute the command is run out */
static int16_t send_command(struct wifi_t *wifi, char *command,
			    const char *expected_response)
{
	uint8_t attempt_count = ATTEMPTS_COUNT;
	int16_t func_ret;
	uint32_t start;

	while (attempt_count > 0) {
		//HAL_UART_AbortTransmit_IT(huart);
		HAL_UART_Transmit_IT(wifi->huart, (uint8_t *)command,
				     strlen(command));

		start = HAL_GetTick();

		/* Wait for transmit completion (or for error), before processing response */
		/* Also, check elapsed time to control timeout */
		while ((UART_TRANSMIT_CPLT_FLAG != 1) &&
		       ((HAL_GetTick() - start < TRANSMIT_TIMEOUT_MS)) &&
		       (UART_ERROR_FLAG != 1))
			;

		/* Process errors */
		if (UART_TRANSMIT_CPLT_FLAG != 1) {
			--attempt_count;
			UART_ERROR_FLAG = 0;
			UART_TRANSMIT_CPLT_FLAG = 0;
			continue;
		}

		/* Reset flag */
		UART_TRANSMIT_CPLT_FLAG = 0;
		UART_ERROR_FLAG = 0;

		/* Process response */
		func_ret = uart_receive_response(wifi);

		if (func_ret < 0) {
			wifi->device_state = WIFI_ERROR;
			--attempt_count;
		} else {
			/*  Do response validation */
			/* If NULL is passed, validation doesn't matter */
			if (expected_response != NULL) {
				/* Remove all CR characters from the response */
				pull_response(wifi->buffer);

				/* Compare response and expected values */
				if (strncmp(wifi->buffer, expected_response,
					    strlen(expected_response)) != 0) {
					--attempt_count;
					wifi->device_state =
						WIFI_NOT_VALID_RESPONSE;
					continue;
				}
			}

			wifi->device_state = WIFI_OK;
			return func_ret;
		}
	}

	/* The number of attempts is run out */
	wifi->device_state = WIFI_ATTEMPTS_END;
	return -1;
}

wifi_state wifi_init(struct wifi_t *wifi, UART_HandleTypeDef *huart_in)
{
	/* Set WIFI_OK to let others functions execute */
	wifi->device_state = WIFI_OK;
	wifi->is_wifi_connected = 0;

	wifi->huart = huart_in;
	wifi->buffer = calloc(BUFFER_SIZE, sizeof(char));
	wifi->buffer_command = calloc(BUFFER_SIZE, sizeof(char));

	/* Disable echoing */
	if (send_command(wifi, AT_DISABLE_ECHO, NULL) < 0) {
		/* If an error occurs, return the device state which is set by send_command function */
		wifi_deinit(wifi);
		return wifi->device_state;
	}

	return WIFI_OK;
}

wifi_state wifi_deinit(struct wifi_t *wifi)
{
	free(wifi->buffer);
	free(wifi->buffer_command);
	return WIFI_OK;
}

wifi_state wifi_connect(struct wifi_t *wifi, char *ssid, char *pass)
{
	/* Set the station mode */
	int16_t response_size = send_command(wifi, AT_SET_STATION_MODE, NULL);

	/* Check the response */
	if (response_size < 1) {
		wifi->is_wifi_connected = 0;
		return wifi->device_state;
	}

	/* Disconnect from the AP if the module has already connected to it because of stored command in flash */
	wifi_state res = wifi_disconnect(wifi);

	/* Check the result of execution */
	if (res != WIFI_OK) {
		wifi->is_wifi_connected = 0;
		return res;
	}

	/* Connect to the wi-fi station */
	memset(wifi->buffer_command, 0, BUFFER_SIZE);
	sprintf(wifi->buffer_command, "%s\"%s\",\"%s\"\r\n", AT_CONNECT_BASE,
		ssid, pass);

	response_size = send_command(wifi, wifi->buffer_command, NULL);

	/* Check the response */
	if (response_size < 1) {
		wifi->is_wifi_connected = 0;
		return wifi->device_state;
	}

	wifi->is_wifi_connected = 1;
	return WIFI_OK;
}

wifi_state wifi_disconnect(struct wifi_t *wifi)
{
	/* Disconnect from the AP */
	int16_t response_size = send_command(wifi, AT_DISCONNECT, NULL);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	return WIFI_OK;
}

wifi_state wifi_get_ip(struct wifi_t *wifi, char *buffer_out)
{
	if (!wifi->is_wifi_connected) {
		return WIFI_ERROR;
	}

	/* Request local network information */
	int16_t response_size = send_command(wifi, AT_GET_IP, NULL);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	/* Get substring with the IP from the response */
	pull_ip(wifi->buffer, buffer_out);

	return WIFI_OK;
}

wifi_state wifi_send_str_tcp(struct wifi_t *wifi, char *address, char *port,
			     char *str, size_t size)
{
	if (!wifi->is_wifi_connected) {
		return WIFI_ERROR;
	}

	/* Connect to the server socket */
	memset(wifi->buffer_command, 0, BUFFER_SIZE);
	sprintf(wifi->buffer_command, "%s\"%s\",%s\r\n", AT_TCP_CONNECT_BASE,
		address, port);
	int16_t response_size = send_command(wifi, wifi->buffer_command, NULL);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	/* Set data length */
	memset(wifi->buffer_command, 0, BUFFER_SIZE);
	__SET_WAIT_FOR_RECEIVE_END__(0);
	sprintf(wifi->buffer_command, "%s%d\r\n", AT_TCP_SEND_BASE, size);

	response_size = send_command(wifi, wifi->buffer_command, NULL);
	__SET_WAIT_FOR_RECEIVE_END__(1);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	/* Send data */
	response_size = send_command(wifi, str, NULL);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	/* Close the connection */
	response_size = send_command(wifi, AT_TCP_CLOSE, NULL);

	/* Check the response */
	if (response_size < 1) {
		return wifi->device_state;
	}

	return WIFI_OK;
}

wifi_state wifi_send_str_to_thingsspeak(struct wifi_t *wifi, float temp, float hum) {
    if (!wifi->is_wifi_connected) {
        return WIFI_ERROR;
    }

//    /* Set single tcp connection mode */
//    memset(wifi->buffer_command, 0, BUFFER_SIZE);
//    int16_t response_size = send_command(wifi, AT_TCP_SET_SINGLE_CON, NULL);
//
//    /* Check the response */
//    if (response_size < 1) {
//        return wifi->device_state;
//    }

    /* Connect to the server socket */
    memset(wifi->buffer_command, 0, BUFFER_SIZE);
    uint8_t response_size = send_command(wifi, AT_TCP_SEND_THINGSSPEAK, NULL);

    /* Check the response */
    if (response_size < 1) {
        return wifi->device_state;
    }

    /* Get the query size */
    memset(wifi->buffer_command, 0, BUFFER_SIZE);
    sprintf(wifi->buffer_command, "GET /update?key=%s&field1=%d.%d&field2=%d\r\n\r\n", THINGSPEAK_API_KEY, (uint8_t) temp, ((uint8_t)(temp * 10)) % 10, (uint8_t) hum);

    size_t command_size = strlen(wifi->buffer_command);

    /* Set data length */
    memset(wifi->buffer_command, 0, BUFFER_SIZE);
    __SET_WAIT_FOR_RECEIVE_END__(0);
    sprintf(wifi->buffer_command, "%s%d\r\n", AT_TCP_SEND_BASE, command_size);

    response_size = send_command(wifi, wifi->buffer_command, NULL);
    __SET_WAIT_FOR_RECEIVE_END__(1);

    /* Check the response */
    if (response_size < 1) {
        return wifi->device_state;
    }

    /* Compose data */
    memset(wifi->buffer_command, 0, BUFFER_SIZE);
    sprintf(wifi->buffer_command, "GET /update?key=%s&field1=%d.%d&field2=%d\r\n\r\n", THINGSPEAK_API_KEY, (uint8_t) temp, ((uint8_t)(temp * 10)) % 10, (uint8_t) hum);

    /* Send data */
    response_size = send_command(wifi, wifi->buffer_command, NULL);

    /* Check the response */
    if (response_size < 1) {
        return wifi->device_state;
    }

    /* Close the connection */
    response_size = send_command(wifi, AT_TCP_CLOSE, NULL);

    /* Check the response */
    if (response_size < 1) {
        return wifi->device_state;
    }

    return WIFI_OK;
}