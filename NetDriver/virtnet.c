#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/inetdevice.h>
#include <linux/moduleparam.h>
#include <net/arp.h>
#include <linux/ip.h>

MODULE_AUTHOR("Bogdan Sidorov");
MODULE_LICENSE("GPL v2");

#define ERR(...) printk(KERN_ERR "! " __VA_ARGS__)
#define LOG(...) printk(KERN_INFO "! " __VA_ARGS__)
#define DBG(...)                                                               \
	if (debug != 0)                                                        \
	printk(KERN_INFO "! " __VA_ARGS__)

static int debug = 0;
module_param(debug, int, 0);

static char *link = "eth0";
module_param(link, charp, 0);

static char *ifname = "virt";
module_param(ifname, charp, 0);
static u32 child_ip = 0; // IP виртуального интерфейса (установленного ifconfig)

static struct net_device *child = NULL;

struct priv {
	struct net_device_stats stats;
	struct net_device *parent;
};

static char *strIP(u32 addr)
{ // диагностика IP в точечной нотации
	static char saddr[MAX_ADDR_LEN];
	sprintf(saddr, "%d.%d.%d.%d", (addr)&0xFF, (addr >> 8) & 0xFF,
		(addr >> 16) & 0xFF, (addr >> 24) & 0xFF);
	return saddr;
}

static int open(struct net_device *dev)
{
	/* Storing the IPv4 address of a child device (not working) */
	// if (dev != NULL) {
	//   if (dev->ip_ptr != NULL) {
	//     struct in_device *in_dev = dev->ip_ptr;
	//
	//     if (in_dev->ifa_list != NULL) {
	//       struct in_ifaddr *ifa = in_dev->ifa_list;      /* IP ifaddr chain */
	//       child_ip = ifa->ifa_address;
	//     }
	//   }
	// }

	/* Storing the IPv4 address of a child device v2 (not working too) */
	// struct in_device *in_dev = rcu_dereference(dev->ip_ptr);
	// // in_dev has a list of IP addresses (because an interface can have multiple)
	// struct in_ifaddr *ifap;
	// for (ifap = in_dev->ifa_list; ifap != NULL; ifap = ifap->ifa_next) {
	// 	child_ip = ifap->ifa_address; // is the IPv4 address
	// 	DBG("CHILD_IP => %lu\n", child_ip);
	// }

	netif_start_queue(dev);
	LOG("%s: device opened", dev->name);
	return 0;
}

static int stop(struct net_device *dev)
{
	netif_stop_queue(dev);
	LOG("%s: device closed", dev->name);
	return 0;
}

static netdev_tx_t start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct priv *priv = netdev_priv(dev);
	priv->stats.tx_packets++;
	priv->stats.tx_bytes += skb->len;
	if (priv->parent) {
		skb->dev = priv->parent;
		skb->priority = 1;
		dev_queue_xmit(skb);

		if (debug != 0) {
			struct iphdr *ip = ip_hdr(skb);
			char daddr[MAX_ADDR_LEN], saddr[MAX_ADDR_LEN];
			strcpy(daddr, strIP(ip->daddr));
			strcpy(saddr, strIP(ip->saddr));
			DBG("tx: from IP=%s to IP=%s with length: %u", saddr,
			    daddr, skb->len);
		}
		return 0;
	}
	return NETDEV_TX_OK;
}

static struct net_device_stats *get_stats(struct net_device *dev)
{
	return &((struct priv *)netdev_priv(dev))->stats;
}

static struct net_device_ops crypto_net_device_ops = {
	.ndo_open = open,
	.ndo_stop = stop,
	.ndo_get_stats = get_stats,
	.ndo_start_xmit = start_xmit,
};

// #define MAX_ADDR_LEN    32  <netdev.h>
// #define ETH_ALEN        6   /* Octets in one ethernet addr   */ <if_ether.h>

static void setup(struct net_device *dev)
{
	int j;

	ether_setup(dev);
	memset(netdev_priv(dev), 0, sizeof(struct priv));
	dev->netdev_ops = &crypto_net_device_ops;

	for (j = 0; j < ETH_ALEN; ++j) // fill in the MAC address with a phoney
		dev->dev_addr[j] = (char)j;
}

/* Network filter.
I don't use it now, because I don't have a clear understanding about
must I use it with the existed rx_handler or that's like an alternative.
The absence of using functions like netif_receive_skb or netif_rx makes me confused
*/

// // обработчик фреймов ETH_P_IP
// int ip4_pack_rcv(struct sk_buff *skb, struct net_device *dev,
// 		 struct packet_type *pt, struct net_device *odev)
// {
// 	skb->dev = child; // передача фрейма в виртуальный интерфейс
// 	int res = netif_rx(skb);
//
// 	struct iphdr *ip = ip_hdr(skb);
// 	struct priv *priv = netdev_priv(child);
// 	priv->stats.rx_packets++;
// 	priv->stats.rx_bytes += skb->len;
// 	if (debug != 0) {
//
// 		char daddr[MAX_ADDR_LEN], saddr[MAX_ADDR_LEN];
// 		strcpy(daddr, strIP(ip->daddr));
// 		strcpy(saddr, strIP(ip->saddr));
// 		DBG("rx: from IP=%s to IP=%s with length: %u", saddr, daddr,
// 		    skb->len);
// 	}
//
// 	return skb->len;
// };
//
// static struct packet_type ip4_proto = {
//   __constant_htons(ETH_P_IP),
//   NULL,
//   ip4_pack_rcv,
// 	(void *)1,
//   NULL };

static rx_handler_result_t handle_frame(struct sk_buff **pskb)
{
	struct sk_buff *skb = *pskb;
	if (child) {
		struct priv *priv = netdev_priv(child);
		priv->stats.rx_packets++;
		priv->stats.rx_bytes += skb->len;

		struct iphdr *ip = ip_hdr(skb);

		char daddr[MAX_ADDR_LEN], saddr[MAX_ADDR_LEN];
		strcpy(daddr, strIP(ip->daddr));
		strcpy(saddr, strIP(ip->saddr));
		DBG("rx: from IP=%s to IP=%s with length: %u", saddr, daddr,
		    skb->len);

		skb->dev = child;
		netif_receive_skb(skb);
		return RX_HANDLER_CONSUMED;
		return RX_HANDLER_ANOTHER;
	}
	return RX_HANDLER_PASS;
}

int __init init(void)
{
	int err = 0;
	struct priv *priv;
	char ifstr[40];

	sprintf(ifstr, "%s%s", ifname, "%d");
	child = alloc_netdev_mqs(sizeof(struct priv), ifstr, NET_NAME_UNKNOWN,
				 setup, 1, 1);

	if (child == NULL) {
		ERR("%s: allocate error", THIS_MODULE->name);
		return -ENOMEM;
	}

	priv = netdev_priv(child);
	priv->parent = __dev_get_by_name(&init_net, link); // parent interface

	if (!priv->parent) {
		ERR("%s: no such net: %s", THIS_MODULE->name, link);
		err = -ENODEV;
		goto err;
	}

	if (priv->parent->type != ARPHRD_ETHER &&
	    priv->parent->type != ARPHRD_LOOPBACK) {
		ERR("%s: illegal net type", THIS_MODULE->name);
		err = -EINVAL;
		goto err;
	}

	/* also, and clone its IP, MAC and other information */
	memcpy(child->dev_addr, priv->parent->dev_addr, ETH_ALEN);
	memcpy(child->broadcast, priv->parent->broadcast, ETH_ALEN);

	if ((err = dev_alloc_name(child, child->name))) {
		ERR("%s: allocate name, error %i", THIS_MODULE->name, err);
		err = -EIO;
		goto err;
	}

	register_netdev(child);

	// ip4_proto.dev = priv->parent;
	// dev_add_pack(&ip4_proto);

	rtnl_lock();
	netdev_rx_handler_register(priv->parent, &handle_frame, NULL);
	rtnl_unlock();

	LOG("module %s loaded", THIS_MODULE->name);
	LOG("%s: create link %s", THIS_MODULE->name, child->name);
	return 0;
err:
	free_netdev(child);
	return err;
}

void __exit exit(void)
{
	struct priv *priv = netdev_priv(child);
	if (priv->parent) {
		rtnl_lock();
		netdev_rx_handler_unregister(priv->parent);
		rtnl_unlock();
		LOG("unregister rx handler for %s\n", priv->parent->name);
	}
	//dev_remove_pack(&ip4_proto);
	unregister_netdev(child);
	free_netdev(child);
	LOG("module %s unloaded", THIS_MODULE->name);
}

module_init(init);
module_exit(exit);
