#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <getopt.h>
#include <time.h>

#define PORT 8080
#define BUFFER_LENGTH 1024

#define RESPONSE_OK "OK"
#define RESPONSE_ERROR "ERROR"

static char *file_path = NULL;
static char *address = NULL;
static int server_fd;
static int client_fd;

static void read_args(int argc, const char *argv[]);
static void free_memory();

int main(int argc, char const *argv[])
{
	read_args(argc, argv);

	if (file_path == NULL || address == NULL) {
		printf("Please, use parameters for setting the socket address and file path!");
		return 0;
	}

	struct sockaddr_in server_address;

	int opt = 1;
	int server_address_length = sizeof(server_address);
	char buffer[BUFFER_LENGTH] = { 0 };

	// Create a server-side socket and get its descriptor
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket creation failed");
		free_memory();
		exit(EXIT_FAILURE);
	}

	// Check the availability of address and port
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt,
		       sizeof(opt))) {
		perror("setsockopt function failed");
		free_memory();
		exit(EXIT_FAILURE);
	}

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr(address);
	server_address.sin_port = htons(PORT);

	// Attach IPv4 address and port to a socket
	if (bind(server_fd, (struct sockaddr *)&server_address,
		 sizeof(server_address)) < 0) {
		perror("binding failed");
		free_memory();
		exit(EXIT_FAILURE);
	}

	// set the queue parameters and wait for the first connection
	if (listen(server_fd, 10) < 0) {
		perror("listening error");
		free_memory();
		exit(EXIT_FAILURE);
	}

	FILE *file_ptr;
	time_t raw_time;
	struct tm *time_info;

	while (1) {
		if ((client_fd = accept(
			     server_fd, (struct sockaddr *)&server_address,
			     (socklen_t *)&server_address_length)) < 0) {
			perror("accepting connection error");
			free_memory();
			exit(EXIT_FAILURE);
		}

		memset(buffer, 0, BUFFER_LENGTH);

		if (recv(client_fd, buffer, BUFFER_LENGTH, 0) < 0) {
			// Reporting a receiving message error to the client
			send(client_fd, RESPONSE_ERROR, strlen(RESPONSE_ERROR),
			     0);
			perror("receive message error");
			free_memory();
			exit(EXIT_FAILURE);
		}

		// saving the message to the file
		file_ptr = fopen(file_path, "a");

		if (file_ptr == NULL) {
			// Reporting an opening file error to the client
			send(client_fd, RESPONSE_ERROR, strlen(RESPONSE_ERROR),
			     0);
			perror("opening file error");
			free_memory();
			exit(EXIT_FAILURE);
		}

		// get data and time
		time(&raw_time);
		time_info = localtime(&raw_time);

		fflush(file_ptr);
		fprintf(file_ptr, "%d:%d:%d;%d:%d:%d;%s\n",
			time_info->tm_year + 1900, time_info->tm_mon,
			time_info->tm_mday, time_info->tm_hour,
			time_info->tm_min, time_info->tm_sec, buffer);

		fclose(file_ptr);

		// printing the message to the terminal
		printf("Received mesg (%d:%d:%d_%d:%d:%d) => %s\n",
		       time_info->tm_year + 1900, time_info->tm_mon,
		       time_info->tm_mday, time_info->tm_hour,
		       time_info->tm_min, time_info->tm_sec, buffer);

		// returning the state of transferring to the client
		send(client_fd, RESPONSE_OK, strlen(RESPONSE_OK), 0);

		fflush(stdout);

		close(client_fd);
	}

	free_memory();
}

static void read_args(int argc, const char *argv[])
{
	if (argv[0] == NULL || argv[1] == NULL) {
		return;
	}

	int c;
	size_t optarg_size;

	char *short_opt = "hf:a:";

	const struct option long_opt[] = {

		{ "help", 0, 0, 'h' },
		{ "file", 1, 0, 'f' },
		{ "address", 1, 0, 'a' },
		{ NULL, 0, NULL, 0 }
	};

	while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (c) {
		case 'h':
			printf("");
			break;
		case 'f':
			optarg_size = strlen(optarg);
			file_path = malloc(sizeof(char) * optarg_size);
			strncpy(file_path, optarg, optarg_size);
			break;
		case 'a':
			optarg_size = strlen(optarg);
			address = malloc(sizeof(char) * optarg_size);
			strncpy(address, optarg, optarg_size);
			break;
		case '?':
			break;
		}
	}
}

static void free_memory()
{
	free(file_path);
	free(address);
	close(server_fd);
}
